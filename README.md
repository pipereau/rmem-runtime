# a Runtime for disaggregated memory

# Context

There has been various hardware proposals to introduce a new memory tier to fill the gap between volatile memory latencies and NVMe latencies.
Among these proposals, multiple research projects have leveraged the recent improvement in networking and fabric IO latency to propose a solution to access memory of another server.

Different remote memory abstractions have been proposed resulting in different trade-offs.
Some implementations have chosen to support **cache coherency** 
All implementation have chosen different transparency levels. Some implementations may require **source code modifications**, or **compilation modifications**, or **runtime modifications**, or **Operating System modification**, or even **Hardware Modifications**.

Our previous work offers transparent remote memory support for anonymous memory of user-space applications.
It leverages Linux Kernel swapping mechanism to move colder pages to remote memory while keeping hotter pages in local memory.
It uses RDMA hardware (infiniband) and exposes a basic abstraction named memory region which is a set of contiguous physical pages. This memory region implements a Read/Write/Alloc/Free API. 
But this approach is problematic for languages using automatic memory management (AMM).
Indeed, AMM algorithms are implemented in language runtime and require traversing the heap to determine which objects must be freed.
Any access to a remote object requires to wait until the object is brought back in local memory which hurts latency. Moreover, it requires multiple IOs between the memory server and the memory client.

Correcting our previous in-kernel implementation is not suitable for multiple reasons:
1. Kernel does not expose locality information to the runtime
2. Garbage collection require knowledge of the heap layout (object size, etc...) which is unknown by kernel as it is language and program dependant.

A better solution is to modify runtime to support accesses to remote memory.

# Subject

We will not rely on any uncommon hardware to access remote memory for multiple reasons:
1. CPU support to access remote memory is too expensive
2. It does not exist on commodity hardware
3. There is no standard yet whether dedicated instructions will be added or if load/store instructions will be transparent

Thus, our proposal should use existing existing RDMA network card which offer shared memory semantics. 

It would be possible to perform modification to a language runtime without modification to the source code to perform remote memory operations.
However, every single object operation would need to be intercepted to determine whether the object is local or not and to select the proper method to access it.
Such a choice would result in serious performance degradation by keeping track of every object used and removing every operation inlining.

## First Step: Remote and local objects

Our proposal is to get help from the developer by exposing two kinds of objects: **remote objects** and **local objects**:
* Local objects are traditional objects stored on the compute node memory and its memory is modified with CPU instructions.
* Remote objects are objects stored on the remote memory node.

A first step would be to support remote object allocations/freeing, write/read using infiniband for a Java runtime in GraalVM for example.

## Second Step: Object locality transitions

Exposing two kind of objects allows for different code path executions.
But for performance reasons a remote object which is often accessed could be upgraded from remote to local. 
Reversely, a local object which is rarely accessed could be transitioned to a remote object.

A second step would be to support transition of an object *from local to remote* and *from remote to local*.

Object locality transitions are non trivial to achieve. Here are some of the problem one can encounter:
* Firstly, let's consider a local object A pointing to a remote object B (`loc(A) -> rem(B)`) When object B is transitioned to a local object, the reference to B in object A needs to be modified to point to a local address so that `loc(A) -> loc(B)`. However the transition has occurred to B, but how does B find the reference to A ? More generally, how can B find all the object referencing him ?
* Secondly, a transition on an object A can occur while this object is read or modified. It is necessary for the transition to be atomic with regards to operations performed simultaneously on the object.
* Thirdly, it is necessary to support two paths of execution according to the object state while taking care that method inlining does not prevent switching remote memory code path to local memory code path.


