all:
	pdflatex slides.tex

clean:
	rm slides.aux  slides.log  slides.nav  slides.out  slides.pdf  slides.snm  slides.toc

view:
	firefox-bin slides.pdf
